# Clyde Real Time Data Feed

The information contained within this document, including but not limited to any product specification, is subject to change without notice.

My Web Application Ltd provides no warranty with regard to this document or any other information contained herein and hereby expressly disclaims any implied warranties of merchantability or fitness for any particular purpose with regard to any of the foregoing.

My Web Application Ltd assumes no liability for any damages incurred directly or indirectly from any technical or typographical errors or omissions contained herein or for discrepancies between the product and the user's guide. In no event shall My Web Application Ltd be liable for any incidental, consequential, special, or exemplary damages, whether based on tort, contract or otherwise, arising out of or in connection with this user's guide or any other information contained herein or the use thereof.

## General

* All methods are POST methods.
* All methods accept JSON data only.
* All methods must be authorized by providing a username and a password in the header.
* All methods must use HTTPS.

Example:
```
$data = array(##SOME DATA ARRAY##);
$data_string = json_encode($data);
$ch = curl_init('https://active.estate/api/v1/clyde/branches'); 
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Username: ###USERNAME###',
    'Password: ###PASSWORD###',
    'Content-Length: '. strlen($data_string))
);
$result = curl_exec($ch);
```

The following example presents the response in case of an error while using one of the methods described below:

Example:
```
{
    "valid": false,
    "errors": {
        "branch_id": [
            "The branch id field is required."
        ]
    },
    "results": null
} 
```

## Get all branches

This method will return all current branches from Active Estate database.

URL: [https://active.estate/api/v1/clyde/branches](https://active.estate/api/v1/clyde/branches) 

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": [
        {
            "id": 84539,
            "name": "Clyde Property - West End",
            "property_number_or_name": "145 ",
            "street_name": "Byres Road",
            "locality": "",
            "town_or_city": "GLASGOW",
            "county": "City of Glasgow",
            "postcode": "G12 8TT"
        },
        {
            "id": 84544,
            "name": "Clyde Property - Ayr",
            "property_number_or_name": "9 ",
            "street_name": "Beresford Terrace",
            "locality": "",
            "town_or_city": "AYR",
            "county": "South Ayrshire",
            "postcode": "KA7 2ER"
        },
        …
    ]
}
```

## Get all properties

This method will return all properties per branch from Active Estate database. The list will include published properties only.

URL: [https://active.estate/api/v1/clyde/properties](https://active.estate/api/v1/clyde/properties)

Parameters:

| Name | Type | Required | Description |
| :--- | :--- | :--- | :--- |
| branch_id | Integer | Yes | Unique Active Estate reference for this branch |

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": [
        {
            "id": 158704,
            "channel_id": 1,
            "agent_ref": "ABC123",
            "updated_at": "2015-11-18 12:09:42"
        },
        …
    ]
}
```

## Add or update a property

This method will handle adding and updating a property based on the “agent_ref” and the “branch_id” parameters.

URL: [https://active.estate/api/v1/clyde/send-property](https://active.estate/api/v1/clyde/send-property)

Parameters:

| Name | Type | Required | Description |
| :--- | :--- | :--- | :--- |
| agent_ref | String | Yes | Agent's unique reference for this property |
| branch_id | Integer | Yes | Unique Active Estate reference for this branch |
| channel_id | Integer | Yes | Defines whether this is sales or lettings channel for a given branch: 1 - Sales 2 - Lettings |
| property_type_id | Integer | Yes | Type of the property being sent. For complete list, see: [https://active.estate/api/v1/lookup/property_types/](http://active.estate/api/v1/lookup/property_types/) |
| status_id | Integer | Yes | The current transaction status for this property. For complete list, see: [https://active.estate/api/v1/lookup/status/](http://active.estate/api/v1/lookup/status/) |
| summary | String (1000) | Yes | Summary description of the property |
| description | String(32K) | Yes | Full description of the property |
| features | String | No | Features of the property. Comma separated items |
| bedrooms | Integer | Yes | Number of bedrooms for the property | 
| bathrooms | Integer | No | Number of bathrooms for the property |
| reception_rooms | Integer | No | Number of reception rooms for the property |
| price| Integer | Yes | Price of the property |
| price_qualifier_id | Integer | Yes | The qualifier on the advertised price of the property. For complete list, see: [https://active.estate/api/v1/lookup/price-qualifiers/](http://active.estate/api/v1/lookup/price-qualifiers/) |
| rent_frequency_id | Integer | No | Frequency of rental payments for the property being sent: 1 - Yearly, 4 - Quarterly, 12 - Monthly, 52 - Weekly, 365 - Daily |
| administration_fee | String (200) | No | Admin fee required for the rental contract of the property |
| let_type_id | Integer | No | Type of rental contract available for this property: 1 - Long term, 2 - Short term, 4 - Commercial, 0 - Not specified |
| minimum_term | Integer | No | Minimum term for the rental contract in months |
| contract_months | Integer | No | Length of rental contract in months |
| date_available | Date | No | Date a rental property is available from in the format: DD/MM/YYYY |
| property_number_or_name | String (60) | Yes | Property name or number, or the first line of the address |
| street_name | String (60) | No | Street name or the second line of the address |
| locality | String (60) | No | Locality or the third line of the address |
| town_or_city | String (60) | Yes | Town in which the property is located |
| county | String (60) | No | County or the fourth line of the address |
| postcode | String (8) | Yes | Full postcode of the property |
| display_address | String (120) | Yes | Address which should be displayed for the property |
| longitude | Decimal | No | Exact longitude of the property |
| latitude | Decimal | No | Exact latitude of the property |
| furnished_type_id | Integer | No | Furnishing status of the rental of the property being sent: 0 - Furnished, 1 - Part-furnished, 2 - Unfurnished, 4 - Furnished/Unfurnished |
| premium | Boolean | No | Indicating premium listing |
| featured | Boolean | No | Indicating featured listing |
| agent_email | Email | No | The email address of the agent |
| media | Array | No | Array of media files (please check the following table for the array parameters) |
| notes | text | No | Notes for the property |

#### Media array structure

| Name | Type | Required | Description |
| :--- | :--- | :--- | :--- |
| media_type | Integer | Yes | Type of media being sent: 1 - Image, 2 - Floorplan, 3 - Brochure, 4 - Virtual Tour, 5 - Audio Tour, 6 - EPC, 7 - EPC Graph |
| media_url | String | Yes | URL to retrieve given piece of media from |
| caption | String (20) | No | Caption to be displayed for given piece of media. |
| sort_order | Integer | Yes | Display order for given piece of media. |
| updated_at | Date Time | Yes | Indicate when the image last updated. Date time format: DD/MM/YYYY HH: mm:ss |

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": {
        "id": 158704,
        "channel_id": 1,
        "agent_ref": "ABC123",
        "updated_at": "2015-11-18 13:27:39"
    }
}
```

## Deleting a property

This method manages deleting a property from Active Estate database.

URL: [https://active.estate/api/v1/clyde/remove-property](https://active.estate/api/v1/clyde/remove-property) 

Parameters:

| Name | Type | Required | Description |
| :--- | :--- | :--- | :--- |
| agent_ref | String | Yes | Unique Agent's reference for given property |
| branch_id | Integer | Yes | Unique Active Estate reference for given branch |

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": "Property has been deleted."
}
```

## Get all agents

This method returns all agents from Active Estate database.

URL: [https://active.estate/api/v1/clyde/agents](https://active.estate/api/v1/clyde/agents)

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": [
        {
            "id": 14,
            "first_name": "Karen",
            "last_name": "Stewart",
            "job_title": "Sales Manager",
            "dob": null,
            "bio": null,
            "image_path": "https://active.estate/storage/5/agents/1e1b909c5606ff959d8853895f09240cb5f161f4.png",
            "telephone": null,
            "mobile": "07974 136 776",
            "email": "karen.stewart@clydeproperty.co.uk",
            "updated_at": "2015-11-17 17:00:47"
        },
        …
    ]
}
```

## Add or update an agent

This method will add or update an agent based on the email address.

URL: [https://active.estate/api/v1/clyde/send-agent](https://active.estate/api/v1/clyde/send-agent)

Parameters:

| Name | Type | Required | Description |
| :--- | :--- | :--- | :--- |
| email | Email | Yes | Agent's unique email address |
| first_name | String (30) | Yes | First name |
| last_name | String (30) | Yes | Last name |
| job_title | String (60) | No | Job title |
| dob | Date | No | Date of birth on the format: DD/MM/YYYY | 
| telephone | String | No | Land line number |
| mobile | String | No | Mobile number |
| bio | String(2000) | No | About the agent |
| picture | URL | No | The URL to retrieve agent’s picture from |

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": {
        "id": 14,
        "first_name": "Karen",
        "last_name": "Stewart",
        "job_title": "Sales Manager",
        "dob": null,
        "bio": null,
        "image_path": "https://active.estate/storage/5/agents/1e1b909c5606ff959d8853895f09240cb5f161f4.png",
        "telephone": null,
        "mobile": "07974 136 776",
        "bio": "About the agent.",
        "email": "karen.stewart@clydeproperty.co.uk",
        "updated_at": "2015-11-17 17:00:47"
    }
}
```

## Delete an agent

This method will delete an agent based on the email address

URL: [https://active.estate/api/v1/clyde/remove-agent](https://active.estate/api/v1/clyde/remove-agent)

Parameters:

| Name | Type | Required | Description |
| :--- | :--- | :--- | :--- |
| email | Email | Yes | Agent's unique email address |

Sample result:
```
{
    "valid": true,
    "errors": null,
    "results": "Agent has been deleted."
}
```

